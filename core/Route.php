<?php

namespace Illuminate;

class Route
{
	protected $routes = [
		'GET'	=> [],
		'POST'	=> []
	];
	
	/**
	 * [load description]
	 * 
	 * @return [type] [description]
	 */
	public static function load()
	{
		$router = new static;

		require __DIR__.'/../routes/web.php';

		return $router;
	}

	/**
	 * [get description]
	 * 
	 * @param  [type] $uri        [description]
	 * @param  [type] $controller [description]
	 * @return [type]             [description]
	 */
	public function get($uri, $controller)
	{
		$uri = trim($uri, '/');
		$this->routes['GET'][$uri] = $controller;
	}

	/**
	 * [post description]
	 * 
	 * @param  [type] $uri        [description]
	 * @param  [type] $controller [description]
	 * @return [type]             [description]
	 */
	public function post($uri, $controller)
	{
		$uri = trim($uri, '/');
		$this->routes['POST'][$uri] = $controller;
	}

	/**
	 * [direct description]
	 * 
	 * @param  [type] $uri         [description]
	 * @param  [type] $requestType [description]
	 * @return [type]              [description]
	 */
	public function direct($uri, $requestType)
	{
		if (array_key_exists($uri, $this->routes[$requestType])) {
			return $this->callAction(
				...explode('@', $this->routes[$requestType][$uri])
			);
		}

		throw new \Exception("No route was defined", 404);
	}

	/**
	 * [callAction description]
	 * 
	 * @param  [type] $controller [description]
	 * @param  [type] $action     [description]
	 * @return [type]             [description]
	 */
	protected function callAction($controller, $action)
	{
		$controller = "\App\\Http\\Controllers\\{$controller}";

		if (! method_exists($controller, $action)) {
			throw new \Exception("{$controller} does not have this {$action} method defined");
			
		}
		$controller = new $controller;
		return $controller->$action();
	}
}
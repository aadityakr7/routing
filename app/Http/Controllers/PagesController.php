<?php
	
namespace App\Http\Controllers;

class PagesController extends Controller
{
	public function showHomePage()
	{
		$title = "Home Page";

		return view('home', compact('title'));
	}

	public function showAboutPage()
	{
		$title = 'About Page';

		return view('about', compact('title'));
	}

	public function showContactUsPage()
	{
		$title = 'Contact Page';

		return view('contact', compact('title'));
	}
}
<?php
	
use Illuminate\{ Route, Request };

Route::load()->direct(Request::uri(), Request::method());

function view($name, $data = null)
{
	extract($data);

	return require __DIR__."/../views/{$name}.view.php";
}
<?php

$router->get('', 'PagesController@showHomePage');
$router->get('/about', 'PagesController@showAboutPage');
$router->get('/contact-us', 'PagesController@showContactUsPage');